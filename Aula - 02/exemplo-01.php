<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Operadores Lógicos</title>
</head>
<body>
    <script>
        console.log("Operadores Lógicos");

        var x = 5, y = 10;

        console.log(x === 5 && y === 10); //true
        console.log(x == '5' && y === 10); //true
        console.log(x === 5 && y === '10'); //false

        console.log(x == 5 || y == 2); //true
        console.log(x === '5' || y == '10'); //true
        console.log(x === '5') || y === '10'; //false

        console.log(x != 5); //false
        console.log(x != 10) //true
    </script>
</body>
</html>