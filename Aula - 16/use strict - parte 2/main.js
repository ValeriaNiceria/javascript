(function() {
    var obj = {
        prop1: {
            prop2: {
                prop3: {
                    prop31: 'prop31',
                    prop32: 'prop32',
                    prop33: 'prop33'
                }
            } 
        }
    };
    console.log(obj);
    console.log(obj.prop1.prop2.prop3);

    with( obj.prop1.prop2.prop3 ) { //Serve para diminuir o tamanho de um objeto
        console.log(prop32);
    }
})();

/*
(function() {
    'use strict';
    var obj = {
        prop1: {
            prop2: {
                prop3: {
                    prop31: 'prop31',
                    prop32: 'prop32',
                    prop33: 'prop33'
                }
            } 
        }
    };

    with( obj.prop1.prop2.prop3 ) { //Serve para diminuir o tamanho de um objeto
        console.log(prop32); // * SyntaxError: strict mode code may not contain 'with' statements
    }
})();
*/

//No escopo global, dentro de funções 'this === undefined'
(function() {
    var Person = function Person(name, lastName) {
        this.name = name;
        this.lastName = lastName;
    };

    console.log(new Person('Maria', 'Augusta')); //Object { name: "Maria", lastName: "Augusta" }
    console.log(Person('João', 'Silva')); //undefined (virá escopo global) - 'use strict' impede que isso ocorra
})();