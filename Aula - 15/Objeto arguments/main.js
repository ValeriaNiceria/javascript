// # Object arguments #
/*
O objeto arguments é como um objeto Array correspondente
aos argumentos passados para uma função.
*/

(function() {

    function myFunction() {
        return arguments;
    }
    console.log(myFunction());

    
    function myFunction(arg1, arg2) {
        return arguments;
    }
    console.log(myFunction(3, 1)); // Arguments { 0: 3, 1: 1, … }


    function myFunction1(arg1, arg2) {
        return arguments[0];
    }
    console.log(myFunction1(9, 3)); // 9


})();