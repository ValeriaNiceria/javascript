(function() {

    /*
       ** this **
       -> Em métodos de objetos: Referência ao objeto
    */

    var myOBject = {
        myProperty: 1,
        init: function init() {
            return this;
        }
    };

    console.log(myOBject);
    console.log('myObject: ', myOBject.init());


    var myOBject1 = {
        myProperty: 10,
        init: function init() {
            return this.myProperty;
        }
    };

    console.log('myObject1: ', myOBject1.init()); //10


    /*
        ** this **
        -> Em funções: 
            Pode ter 2 valores.
        - Referência ao objeto global.
    */

    function myFunction() {
        return this;
    }

    console.log('myFunction: ', myFunction()); //Window
    console.log(myFunction() === window); //true


     /*
        ** this **
        -> Em funções: 
            Pode ter 2 valores.
        - Referência do objeto instanciado
    */

    var newObject = new Object();
    console.log('newObject', newObject); //Object { }


    //Criando um construtor

    function MyConstructor() {
        this.prop1 = 'prop1';
        this.prop2 = 'prop2';
    }

    var constructor = new MyConstructor();

    console.log('MyConstructor ', constructor); //Object { prop1: "prop1", prop2: "prop2" }



})();